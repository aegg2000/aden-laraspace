@extends('admin.layouts.layout-basic')

@section('content')
    <div class="main-content">
        <div class="page-header">
            <h3 class="page-title">Roles</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{route('roles.index')}}">Roles</a></li>
                <li class="breadcrumb-item active">Create</li>
            </ol>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h6>Crear Rol</h6>
                    </div>
                    <form action="{{ route('roles.store') }}" method="post" id="formulario">
                        @csrf
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group row">
                                        <label for="nombre" class="col-form-label col-sm-3">Nombre</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" name="nombre" placeholder="Ingrese el nombre" required>
                                        </div>
                                    </div>
                                </div>
                            </div>                        
                        </div>
                        <div class="card-footer">
                            <a href="{{ route('roles.index') }}" class="btn btn-secondary btn-pressable">Volver</a>
                            <button type="submit" class="btn btn-success btn-pressable">Crear</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop
