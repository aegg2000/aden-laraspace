@extends('admin.layouts.layout-basic')

@section('scripts')
<script>
    $(document).ready(function () {
        
        $('#users-datatable').DataTable({
            responsive: true
        });
        
        $('[data-confirmation="notie"]').on('click', function (e) {
            e.preventDefault();
            
            var reg = $(this);

            notie.confirm({
                text: 'Esta seguro de eliminar?',
                submitText: 'Si', 
                cancelText: 'Cancelar',
                submitCallback: function () {
                    deleteUser(reg);
                }
            });
            return false
        });

        function deleteUser (reg) {
            console.log(reg);
            var url = reg.attr('href');
            var token = reg.data('token');
            console.log(url);
            $.ajax({
                type: 'POST',
                data: {_method: 'delete', _token: token},
                url: url,
                success: function (data) {
                    toastr['success']('User Deleted', 'Success');
                    $(reg).closest('tr').remove();
                },
                error: function (data) {
                    toastr['error']('There was an error', 'Error')
                }
            });
        }  
    });  
</script>
@stop

@section('content')
    <div class="main-content">
        <div class="page-header">
            <h3 class="page-title">Users</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{route('users.index')}}">Users</a></li>
                <li class="breadcrumb-item active">Users</li>
            </ol>
            <div class="page-actions">
                <a href="{{ route('users.create') }}" class="btn btn-primary"><i class="icon-fa icon-fa-plus"></i> Nuevo usuario</a>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h6>All Users</h6>

                        <div class="card-actions">

                        </div>
                    </div>
                    <div class="card-body">
                        <table id="users-datatable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Role</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($users as $user)
                                <tr>
                                    <td>{{$user->name}}</td>
                                    <td>{{$user->email}}</td>
                                    <td>{{$user->rol->nombre}}</td>
                                    <td>
                                        <a href="{{route('users.edit',$user)}}" class="btn btn-default btn-sm"><i class="icon-fa icon-fa-edit"></i> Edit</a>
                                        <a href="{{route('users.destroy',$user->id)}}" class="btn btn-default btn-sm" data-token="{{csrf_token()}}" data-confirmation="notie"> <i class="icon-fa icon-fa-trash"></i> Delete</a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
