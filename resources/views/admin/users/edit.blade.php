@extends('admin.layouts.layout-basic')

@section('content')
    <div class="main-content">
        <div class="page-header">
            <h3 class="page-title">Users</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{route('users.index')}}">Users</a></li>
                <li class="breadcrumb-item active">Edit</li>
            </ol>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h6>Editar Usuario</h6>
                    </div>
                    <form action="{{ route('users.update', $user->id) }}" method="POST">
                        @method('patch')
                        @csrf
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group row">
                                        <label for="nombre" class="col-form-label col-sm-3">Nombre</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" name="nombre" placeholder="Ingrese el nombre" value="{{ $user->name }}" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group row">
                                        <label for="rol" class="col-form-label col-sm-3">Rol</label>
                                        <div class="col-sm-9">
                                            <select name="rol" class="form-control ls-select2" required>
                                                <option value="">Seleccione una opción</option>
                                                @foreach($roles as $rol)
                                                    @if($user->rol_id == $rol->id)
                                                        <option value="{{ $rol->id }}" selected>{{ $rol->nombre }}</option>
                                                    @else
                                                        <option value="{{ $rol->id }}">{{ $rol->nombre }}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group row">
                                        <label for="email" class="col-form-label col-sm-3">Email</label>
                                        <div class="col-sm-9">
                                            <input type="email" class="form-control" name="email" placeholder="Ingrese el correo electrónico" value="{{ $user->email }}" required>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>                        
                        </div>
                        <div class="card-footer">
                            <a href="{{ route('users.index') }}" class="btn btn-secondary btn-pressable">Volver</a>
                            <button type="submit" class="btn btn-success btn-pressable">Actualizar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop
