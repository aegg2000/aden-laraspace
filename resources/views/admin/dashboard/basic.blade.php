@extends('admin.layouts.layout-basic')

@section('content')
    <div class="main-content" id="dashboardPage">
        <div class="row">
            <div class="col-md-12">
                <h3 class="text-center">Bienvenido {{ \Auth()->user()->name }}</h3>
            </div>
        </div>
    </div>
@stop
