<?php

use Illuminate\Database\Seeder;
use Laraspace\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'email' => 'admin@laraspace.in',
            'name' => 'Adrian Gonzalez',
            'rol_id' => '1',
            'password' => bcrypt('123456')
        ]);

        User::create([
            'email' => 'user@laraspace.in',
            'name' => 'Adrian User Gonzalez',
            'rol_id' => '2',
            'password' => bcrypt('123456')
        ]);
    }
}
