<?php

use Illuminate\Database\Seeder;
use Laraspace\Rol;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Rol::create([
            'nombre' => 'Administrador',
        ]);
        Rol::create([
            'nombre' => 'Usuario',
        ]);
    }
}
