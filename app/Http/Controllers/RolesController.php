<?php
namespace Laraspace\Http\Controllers;

use Illuminate\Http\Request;
use Laraspace\Rol;

use DB;

class RolesController extends Controller
{
    public function index()
    {
        $roles = Rol::all();

        return view('admin.roles.index')->with('roles', $roles);
    }

    public function create()
    {
        return view('admin.roles.create');
    }

    public function store(Request $rq)
    {
        try{
            DB::beginTransaction();
            $rol = new Rol();
            $rol->nombre = $rq->nombre;
            $rol->save();
            flash()->success('Rol creado exitosamente');
            DB::commit();
            return redirect()->route('roles.index');
        }catch(\Exception $e){
            DB::rollback();
            dd($e);
            flash()->warning('Error al intentar crear rol');
            return redirect()->route('roles.create');
        }
    }

    public function edit(Rol $role)
    {
        return view('admin.roles.edit', compact('role'));
    }

    public function update(Request $rq, Rol $role)
    {
        try{
            DB::beginTransaction();
            $role->nombre = $rq->nombre;
            $role->save();
            flash()->success('Rol actualizado exitosamente');
            DB::commit();
            return redirect()->route('roles.index');
        }catch(\Exception $e){
            DB::rollback();
            flash()->warning('Error al intentar actualizar rol');
            return redirect()->route('roles.edit', $role);
        }
    }

    public function destroy($id)
    {
        $rol = Rol::findOrFail($id);
        $rol->delete();
        flash()->success('Rol Deleted');

        return redirect()->back();
    }
}
