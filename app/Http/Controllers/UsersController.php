<?php
namespace Laraspace\Http\Controllers;

use Illuminate\Http\Request;
use Laraspace\User;
use Laraspace\Rol;

use DB;

class UsersController extends Controller
{
    public function index()
    {
        $users = User::all();

        return view('admin.users.index')->with('users', $users);
    }

    public function create()
    {
        $roles = Rol::all();
        return view('admin.users.create', compact('roles'));
    }

    public function store(Request $rq)
    {
        try{
            DB::beginTransaction();
            $user = new User();
            $user->name = $rq->nombre;
            $user->rol_id = $rq->rol;
            $user->email = $rq->email;
            $user->password = bcrypt($rq->passw);
            $user->save();
            flash()->success('Usuario creado exitosamente');
            DB::commit();
            return redirect()->route('users.index');
        }catch(\Exception $e){
            DB::rollback();
            flash()->warning('Error al intentar crear usuario');
            return redirect()->route('users.create');
        }
    }

    public function edit(User $user)
    {
        $roles = Rol::all();
        return view('admin.users.edit', compact('user', 'roles'));
    }

    public function update(Request $rq, User $user)
    {
        try{
            DB::beginTransaction();
            $user->name = $rq->nombre;
            $user->rol_id = $rq->rol;
            $user->email = $rq->email;
            $user->save();
            flash()->success('Usuario actualizado exitosamente');
            DB::commit();
            return redirect()->route('users.index');
        }catch(\Exception $e){
            DB::rollback();
            flash()->warning('Error al intentar actualizar usuario');
            return redirect()->route('users.edit', $user);
        }
    }

    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete();
        flash()->success('User Deleted');

        return redirect()->back();
    }
}
